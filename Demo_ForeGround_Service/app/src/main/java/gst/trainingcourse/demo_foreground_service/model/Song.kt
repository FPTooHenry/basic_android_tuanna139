package gst.trainingcourse.demo_foreground_service.model

import java.io.Serializable

data class Song(var title: String, var single: String, var image: Int, var resource: Int) :Serializable {
}