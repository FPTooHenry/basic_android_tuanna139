package gst.trainingcourse.gst_lesson1_ex1_tuanna139_2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if ("text/plain" == intent.type) {
            handleSendText(intent)
        }
    }

    private fun handleSendText(intent: Intent) {
        val txt = intent.getStringExtra(Intent.EXTRA_TEXT)
        if(txt != null){
            activity_txt_intent.text = txt
        }
    }
}