package com.example.boundservice

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.widget.Button
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.concurrent.thread

class MainActivity : AppCompatActivity() {
    companion object {
        const val KEY_RESULT = "data"
    }
    private var total: Int = 0
    private lateinit var mService: MusicBoundService
    private var mBound: Boolean = false
    private var serViceConnection = object : ServiceConnection{
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            var binder = service as MusicBoundService.MyBinder
            mService = binder.getService()
            total = sumNumber()
            onRuntime()
            mBound = true
        }
        override fun onServiceDisconnected(name: ComponentName?) {
            mBound = false
        }

    }

    fun onRuntime()= runBlocking {
        launch(Dispatchers.Default) {
            delay(5000)
            nextFragment(total)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btnStart:Button = findViewById(R.id.btn_start_service)
        var btnStop:Button = findViewById(R.id.btn_stop_service)
        var btnSum:Button = findViewById(R.id.btn_sum)

        btnStart.setOnClickListener{
            onClickStartSerVice()
        }
        btnStop.setOnClickListener {
            onclickStopSerVice()
        }
        btnSum.setOnClickListener {
            onClickStartSerVice()
        }
    }



    private fun nextFragment(total: Int) {
       // val mFragmentManager = supportFragmentManager
       // val mFragmentTransaction = mFragmentManager.beginTransaction()
        val bundle = Bundle()
        bundle.putInt(KEY_RESULT, total)
        var fragmentResult= FragmentResult()
        fragmentResult.arguments = bundle
       // mFragmentTransaction.add(R.id.fragment_sum,fragmentResult).commit()
        supportFragmentManager.beginTransaction()
            .add(R.id.main_activity_frame_layout, fragmentResult, FragmentResult::class.java.name)
            .commit()

    }


    private fun onclickStopSerVice() {
        unbindService(serViceConnection)
        mBound = false
    }

    private fun onClickStartSerVice() {
        var intent = Intent(this, MusicBoundService::class.java)
        bindService(intent, serViceConnection, Context.BIND_AUTO_CREATE )
    }
    fun sumNumber():Int{
        var sum: Int = 0
        for (i in 1..1000000){
            sum += i
        }
        return sum
    }
}